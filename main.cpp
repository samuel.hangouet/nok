#include <iostream>
#include <fstream>
#include <nok/Parser.h>
#include <curses.h>
#include <math/math.h>
#include <console/console.h>
#include <parser/parser.h>

using namespace nok;

bool throws = false;
std::vector<std::string> files;
Parser p;

std::pair<PrimitiveBase *,bool> protectedParse(const std::string & path, Object * scope)
{
    if (throws)
        return {p.parseFile(path, scope),true};

    try
    {
        return {p.parseFile(path, scope),true};
    }
    catch (ell::Error & e)
    {
        std::cerr << e.what() << "\n";
        return {nullptr,false};
    }
}

int run()
{
    auto root = new Object(nullptr);

    root->create("parser", modules::parser::load());
    root->create("math", modules::math::load());
    root->create("console", modules::console::load());

    if (files.empty())
        files.push_back("scripts/shell.nok");

    for (auto path: files)
    {
        bool expectError = path.find(".err") == path.size() - 4;

        auto value = protectedParse(path, root);
        if (value.second == false)
        {
            if (expectError)
                std::cerr << "Expected error happens\n";
            else
                return 1;
        }
        else if (expectError)
        {
            std::cerr << "Expected error did not happen\n";
            return 1;
        }
    }

    return 0;
}

int main(int argc, const char **argv)
{
    for (int i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if (arg[0] != '-')
        {
            files.push_back(arg);
            continue;
        }

        if (arg == "-e")
            throws = true;
        else if (arg == "-t")
            p.showTrace();
        else
        {
            std::cerr << "Unknown option '" << arg << "'. Possible options are:\n"
                      << " -e   throws exception on error\n"
                      << " -t   dumps parser trace in stderr\n";
            return 1;
        }
    }

    return run();
}
