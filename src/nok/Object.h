#pragma once
#include <map>
#include "PrimitiveBase.h"
#include "String.h"
#include "List.h"
#include "Null.h"
namespace nok
{
    struct Interrupt
    {
        enum
        {
            None,
            Return,
            Break,
            Continue
        } kind;

        PrimitiveBase * returnValue = nullptr;
        Object * owner = nullptr;
    };

    // The only mutable primitive
    struct Object : public PrimitiveBase
    {
        struct KeyComparer
        {
            bool operator () (const PrimitiveBase * l, const PrimitiveBase * r) const
            {
                auto lt = l->type();
                auto rt = r->type();
                if (lt != rt)
                    return lt < rt;
                return l->compareSameType(r) < 0;
            }
        };

        std::map<const PrimitiveBase *, PrimitiveBase *, KeyComparer> _map;

        Interrupt _interrupt = {};
        Object * _parent = nullptr;
        Object * _base = nullptr;

        Object(Object * parent)
          : _parent(parent)
        { }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << '{';
            if (! parents.insert(this).second)
            {
                os << "...}";
                return;
            }

            bool first = true;
            for(auto&[key,value]: _map)
            {
                if (first)
                    first = false;
                else
                    os << ", ";
                auto s = static_cast<const String*>(key);
                if (key->type() == Types::String && s->validIdentifier())
                    os << s->value();
                else
                    key->dump(os, parents);
                os << ": ";
                value->dump(os, parents);
            }
            os << '}';
            if (_base != nullptr)
            {
                os << "=>";
                _base->dump(os, parents);
            }

            parents.erase(this);
        }

        Object * toObject(const NodeBase *) override { return this; }

        void foreach(const NodeBase * origin, std::function<bool(PrimitiveBase*)> callback) const override
        {
            for(auto [key, value]: _map)
            {
                auto l = new List({const_cast<PrimitiveBase*>(key), value});
                if (!callback(l))
                    break;
            }
        }

        PrimitiveBase * find(const NodeBase * origin, const PrimitiveBase * needle) const override
        {
            for (auto &p: _map)
            {
                auto key = p.first;
                if (key->type() != needle->type())
                    continue;
                if (key->compareSameType(needle) == 0)
                    return const_cast<PrimitiveBase*>(key);
            }
            return new Null;
        }

        PrimitiveBase * at(const NodeBase * origin, const PrimitiveBase * key) const override
        {
            auto it = _map.find(const_cast<PrimitiveBase *>(key));
            if (it == _map.end())
                throw origin->error() << "No such " << *key;
            return it->second;
        }

        virtual PrimitiveBase * create(const NodeBase * keyOrigin, const PrimitiveBase * key, PrimitiveBase * value)
        {
            auto r = _map.emplace(key, value);
            if (r.second == false)
                throw keyOrigin->error() << "Key already exists";
            return value;
        }

        void create(const std::string & name, PrimitiveBase * value)
        {
            auto r = _map.emplace(new String(name), value);
            if (r.second == false)
                throw;
        }

        PrimitiveBase ** get(const PrimitiveBase * key)
        {
            auto it = _map.find(key);
            if (it == _map.end())
                return nullptr;
            return &it->second;
        }

        PrimitiveBase * set(const NodeBase * keyOrigin, const PrimitiveBase * key, PrimitiveBase * value)
        {
            auto it = _map.find(key);
            if (it == _map.end())
                throw keyOrigin->error() << "Key not found. Do you mean `:` instead?";
            it->second = value;
            return value;
        }

        virtual PrimitiveBase * setOrCreate(const PrimitiveBase * key, PrimitiveBase * value)
        {
            auto r = _map.emplace(key, value);
            r.first->second = value;
            return value;
        }

        virtual PrimitiveBase * getOrCreate(const PrimitiveBase * key, PrimitiveBase * value)
        {
            auto r = _map.emplace(key, value);
            return r.first->second;
        }

        std::pair<PrimitiveBase**,Object*> lookupMember(const PrimitiveBase * key)
        {
            TRACE(" Lookup member " << *key << " into " << *this);
            auto it = _map.find(key);
            if (it != _map.end())
                return {& it->second, this};
            for (auto p = _base; p != nullptr; p = p->_base)
            {
                TRACE("  Fallback to " << *p);
                it = p->_map.find(key);
                if (it != p->_map.end())
                    // Member found in base is still considered located in this object
                    // This explains the difference between base and parent object
                    return {& it->second, this};
            }
            return {nullptr,nullptr};
        }

        std::pair<PrimitiveBase**,Object*> lookup(const PrimitiveBase * key)
        {
            TRACE("Lookup " << *key << " from " << *this);
            auto p = lookupMember(key);
            if (p.first == nullptr && _parent != nullptr)
                return _parent->lookup(key);
            return p;
        }

        int size(const NodeBase * origin) const override
        {
            return _map.size();
        }

        Types type() const override { return Types::Object; }

        virtual Object * owner() const { return nullptr; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            return (o < this) - (this < o);
        }

        void setBase(const NodeBase * op, Object * base)
        {
            for (auto f = base; f != nullptr; f = f->_base)
            {
                if (f == this)
                    throw op->error() << "Circular base assignation";
            }
            _base = base;
        }

        PrimitiveBase * getBase(const NodeBase * op) const
        {
            if (_base == nullptr)
                return new Null;
            return _base;
        }
    };

    // To store function parameters and induction variables
    struct InvisibleObject : public Object
    {
        InvisibleObject(Object * parent)
          : Object(parent),
            _locked(false)
        { }

        PrimitiveBase * create(const NodeBase * keyOrigin, const PrimitiveBase * key, PrimitiveBase * value) override
        {
            if (_locked)
                return _parent->create(keyOrigin, key, value);
            return Object::create(keyOrigin, key, value);
        }

        PrimitiveBase * setOrCreate(const PrimitiveBase * key, PrimitiveBase * value) override
        {
            if (_locked)
                return _parent->setOrCreate(key, value);
            return Object::setOrCreate(key, value);
        }

        PrimitiveBase * getOrCreate(const PrimitiveBase * key, PrimitiveBase * value) override
        {
            if (_locked)
                return _parent->getOrCreate(key, value);
            return Object::getOrCreate(key, value);
        }

        bool _locked;
    };
}
