#pragma once
#include <functional>
#include <vector>
#include <map>
#include "Error.h"
namespace nok
{
    struct Type : public PrimitiveBase
    {
        Type(Types type) : _type(type) { }

        void dump(std::ostream & os) override { os << _type; }

        Types type() override { return Types::Type; }

        int compareSameType(PrimitiveBase * o) override
        {
            auto other = static_cast<Type *>(o);
            return (other->_type < _type) - (_type < other->_type);
        }

        const Types _type;

        Type * asType() override { return this; }
    };
}
