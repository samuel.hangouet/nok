#pragma once
#include "PrimitiveBase.h"
#include "Numbers.h"
#include "Null.h"
#include <vector>
namespace nok
{
    struct List : public PrimitiveBase
    {
        List()
        { }

        List(std::vector<PrimitiveBase*> && values)
          : _content(values)
        { }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << '[';
            for(int i = 0; i < size(); ++i)
            {
                if (i != 0)
                    os << ", ";
                _content[i]->dump(os, parents);
            }
            os << ']';
        }

        void foreach(const NodeBase * origin, std::function<bool(PrimitiveBase*)> callback) const override
        {
            for (auto n: _content)
            {
                if (! callback(n))
                    break;
            }
        }

        PrimitiveBase * copy(const NodeBase * origin, int first, int last) const override
        {
            auto l = new List;
            if (first < (int)_content.size() && last >= first)
                l->_content = std::vector<PrimitiveBase*>(_content.begin(), _content.begin() + (last - first + 1));
            return l;
        }

        PrimitiveBase * concat(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            auto l = new List;
            l->_content = _content;
            if (other->type() == Types::List)
            {
                auto otherList = static_cast<const List*>(other);
                l->_content.insert(l->_content.end(), otherList->_content.begin(), otherList->_content.end());
            }
            else
                l->_content.push_back(const_cast<PrimitiveBase*>(other));
            return l;
        }

        PrimitiveBase * find(const NodeBase * origin, const PrimitiveBase * needle) const override
        {
            for (int i = 0; i < size(); ++i)
            {
                auto n = _content[i];
                if (n->type() != needle->type())
                    continue;

                if (n->compareSameType(needle) == 0)
                    return new Int(i);
            }
            return new Null;
        }

        List * toList(const NodeBase *) override { return this; }

        int size(const NodeBase * origin) const  override
        {
            return (int)_content.size(); 
        }

        int size() const { return _content.size(); }

        PrimitiveBase * at(const NodeBase * origin, int i) const
        {
            if (i >= size())
                throw origin->error() << "Out of bounds";
            return _content[i];
        }

        PrimitiveBase * at(const NodeBase * origin, const PrimitiveBase * index) const override
        {
            return at(origin, index->toInt(origin)->_value);
        }

        Types type() const override { return Types::List; }

        int compareSameType(const PrimitiveBase * other) const override
        {
            auto o = static_cast<const List*>(other);
            int s = (o->_content.size() < _content.size()) - (_content.size() < o->_content.size());
            if (s != 0)
                return s;

            for (int i = 0; i < size(); ++i)
            {
                auto l = _content[i];
                auto r = o->_content[i];
                auto lt = l->type();
                auto rt = r->type();
                s = (rt < lt) - (lt < rt);
                if (s != 0)
                    return s;
                s = l->compareSameType(r);
                if (s != 0)
                    return s;
            }

            return 0;
        }

        std::vector<PrimitiveBase *> _content;
    };
}
