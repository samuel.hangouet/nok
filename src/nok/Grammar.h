#define A(code) .action([this]{ code; })
#define AP(type, code) .action<type>([this](const type & v){ code; })
#define LOC .action<ell::Location>([this](const ell::Location & l){ back()->_location = pinLocation(l); })

// Primitives

#define K(kwd) auto kw_##kwd = nosfx(str(#kwd), NEXT_CHAR);
#include "Keywords.h"
#undef K

FIRST_CHAR = (chset("a-zA-Z_") | range(0x80, 0xFF));

NEXT_CHAR = (chset("a-zA-Z0-9_") | range(0x80, 0xFF));

IDENTIFIER = noskip((FIRST_CHAR >> * NEXT_CHAR) - (
#define K(kwd) kw_##kwd |
#include "Keywords.h"
#undef K
            nop));
IDENTIFIER.setName("identifier");

INTEGRAL = noskip(nosfx( ch('0') >> ( (ch('x') | ch('X')) >> integer<16>() AP(int, _int = v)
                                    | integer<8>() AP(int, _int = v)
                                    | eps A(_int = 0))
                       | integer<>() AP(int, _int = v)
                       , DOT));

STRING = noskip(ch('"') A(_string.clear()) >> * STRING_CHAR >> ch('"'));

STRING_CHAR = ch('\\') >> ( ch('a') A(_string.push_back('\a'))
                          | ch('b') A(_string.push_back('\b'))
                          | ch('f') A(_string.push_back('\f'))
                          | ch('n') A(_string.push_back('\n'))
                          | ch('r') A(_string.push_back('\r'))
                          | ch('t') A(_string.push_back('\t'))
                          | ch('v') A(_string.push_back('\v'))
                          | ch('\\') A(_string.push_back('\\'))
                          | ch('\'') A(_string.push_back('\''))
                          | ch('"') A(_string.push_back('\"'))
                          | ch('?') A(_string.push_back('\?'))
                          | integer<8, 1, 3, unsigned>() AP(unsigned, _string.push_back(v))
                          | ch('x') >> integer<16, 2, 2, unsigned>() AP(unsigned, _string.push_back(v))
                          | eps A(_string.push_back('\\'))
                          )
            | (any AP(char, _string.push_back(v)) - (ch('\n') | ch('\"')));

NUMBER = INTEGRAL A(pushConstant<Int>(_int)) LOC
       | real AP(double, pushConstant<Double>(v)) LOC;
NUMBER.setName("number");

COLON = nosfx(ch(':'), chset("?="));

PLUS = nosfx(ch('+'), ch('+'));

MINUS = nosfx(ch('-'), chset("->"));

DOT = nosfx(ch('.'), ch('.'));

NOT = nosfx(ch('!'), ch('='));
 
GT = nosfx(ch('>'), chset(">="));

LE = nosfx(str("<="), ch('>'));

LT = nosfx(ch('<'), chset("<="));
 
AMP = nosfx(ch('&'), ch('&'));
 
BWXOR = nosfx(ch('^'), ch('^'));
 
BWOR = nosfx(ch('|'), ch('|'));

EQUAL = nosfx(ch('='), chset(">="));

// White spaces, new lines and comments skipping

skipper = chset(" \t\r")
        | str("'''") >> *(any - str("'''")) >> (str("'''") | error("unterminated multi-line comment"))
        | ch('\'') >> *(any - ch('\n'));

nl_skipper = ch('\n') | skipper;

// Syntax

listOrRange = ch('[') >> skip(nl_skipper, expression >> ( COLON >> expression >> ch(']') A(pushBinary(new RangeOperator))
                                                        | ch(']') A(pushUnary(new ListNode))
                                                        | ch(',') A(pushUnary(new ListNode))
                                                          >> expression A(addChild())
                                                          >> * (ch(',') >> expression A(addChild()))
                                                          >> ch(']'))
                                        | ch(']') A(push(new ListNode)));


object = ch('{') A(push(new ObjectNode)) >> statement_list >> ch('}');
object.setTraceName("object");

atome = kw_true A(pushConstant<Bool>(true)) LOC
      | kw_false A(pushConstant<Bool>(false)) LOC
      | kw_null A(pushConstant<Null>()) LOC
      | kw_this A(push(new Variable("this"))) LOC
      | IDENTIFIER AP(std::string, push(new Variable(v))) LOC
      | NUMBER
      | STRING A(pushConstant<String>(_string)) LOC
      | listOrRange LOC
      | object LOC
      | ch('(') >> skip(nl_skipper, (expression | eps A(pushConstant<Null>())) >> ch(')'));
atome.setName("expression");
atome.setTraceName("atome");

arg_list = ch('(') A(push(new ListNode)) LOC
           >> skip(nl_skipper, ! (expression A(addChild()) % ch(','))
                               >> ch(')'));
arg_list.setTraceName("arg_list");

access_or_copy = ch('[') >> skip(nl_skipper, ( COLON A(pushConstant<Int>(0); pushBinary(new CopyOperator))
                                               >> ! expression A(addChild())
                                             | expression >> ( COLON A(pushBinary(new CopyOperator)) >> ! expression A(addChild())
                                                             | eps A(pushBinary(new Access))))
                                             >> ch(']'));
access_or_copy.setTraceName("access_or_copy");

assignable = atome >> * ( access_or_copy
                        | DOT >> IDENTIFIER AP(std::string, push(new Variable(v)); pushBinary(new Member))
                        | arg_list A(pushBinary(new Call))
                        ) LOC;
assignable.setTraceName("assignable");

prefixed = assignable
         | (NOT | kw_not) >> prefixed A(pushUnary(new LnotOperator))
         | PLUS >> NUMBER
         | MINUS >> prefixed A(pushUnary(new MinusOperator))
         | ch('#') >> prefixed A(pushUnary(new SizeOperator))
         | ch('~') >> prefixed A(pushUnary(new BitInvOperator));
prefixed.setTraceName("prefixed");

product = prefixed >> * ( ch('*') >> prefixed A(pushBinary(new MulOperator))
                        | ch('%') >> prefixed A(pushBinary(new ModuloOperator))
                        | ch('/') >> prefixed A(pushBinary(new DivOperator))) LOC;
product.setTraceName("product");

sum = product >> * ( PLUS >> product A(pushBinary(new AddOperator))
                   | MINUS >> product A(pushBinary(new SubOperator))) LOC;
sum.setTraceName("sum");

shift = sum >> *( str(">>") >> sum A(pushBinary(new ShiftRightOperator))
                | str("<<") >> sum A(pushBinary(new ShiftLeftOperator))) LOC;

concat = shift >> * (str("..") >> shift A(pushBinary(new ConcatOperator))) LOC;

search = concat >> * (ch('@') >> concat A(pushBinary(new SearchOperator))) LOC;

threeWay = search >> * (str("<=>") >> search A(pushBinary(new CompareOperator))) LOC;

comparison = threeWay >> !( str("==") >> threeWay A(pushBinary(new EqOperator))
                          | str("!=") >> threeWay A(pushBinary(new NeqOperator))
                          | str(">=") >> threeWay A(pushBinary(new GeOperator))
                          | GT >> threeWay A(pushBinary(new GtOperator))
                          | LE >> threeWay A(pushBinary(new LeOperator))
                          | LT >> threeWay A(pushBinary(new LtOperator))) LOC;

bwand = comparison >> *(AMP >> comparison A(pushBinary(new BitAndOperator))) LOC;

bwxor = bwand >> *(BWXOR >> bwand A(pushBinary(new BitXorOperator))) LOC;

bwor = bwxor >> *(BWOR >> bwxor A(pushBinary(new BitOrOperator))) LOC;
                      
land = bwor >> * ((str("&&") | kw_and) >> bwor A(pushBinary(new LogicalAndOperator))) LOC;

lxor = land >> * ((str("^^") | kw_xor) >> land A(pushBinary(new LogicalXorOperator))) LOC;

lor = lxor >> * ((str("||") | kw_or) >> lxor A(pushBinary(new LogicalOrOperator))) LOC;

base = lor >> ! (str("=>") >> *ch('\n') >> base A(pushBinary(new SetBaseOperator))) LOC;

expression = base >> ! ( str("->") >> *ch('\n') >> statement A(pushFunction())) LOC;
expression.setName("expression");

assignation = expression >> ! ( str(":?") >> *ch('\n') >> assignation A(pushAssign(new GetOrCreateOperator)) 
                              | str(":=") >> *ch('\n') >> assignation A(pushAssign(new SetOrCreateOperator))
                              | ch(':') >> *ch('\n') >> assignation A(pushAssign(new CreateOperator))
                              | EQUAL >> *ch('\n') >> assignation A(pushAssign(new SetOperator))
                              | str("++") A(pushUnary(new IncOperator))
                              | str("--") A(pushUnary(new DecOperator))) LOC;
assignation.setTraceName("assignation");

assign_list = assignation >> ! ( ch(',') A(pushUnary(new ListNode))
                                 >> assignation A(addChild())
                                 >> * (ch(',') >> assignation A(addChild())));
assign_list.setTraceName("assign_list");

statement = ( kw_return A(push(new Return)) >> ! expression A(addChild()) 
            | kw_throw A(push(new Throw)) >> ! expression A(addChild())
            | kw_if >> expression >> *ch('\n') >> statement A(pushBinary(new If))
                    >> ! (fallback(*ch('\n') >> kw_else) >> *ch('\n') >> statement A(addChild()))
            | kw_print >> expression A(pushUnary(new Print))
            | kw_for >> expression >> COLON >> expression >> *ch('\n') >> statement A(pushFor())
            | kw_break A(push(new Break))
            | kw_continue A(push(new Continue))
            | kw_while >> expression >> *ch('\n') >> statement A(pushBinary(new While))
            | assign_list ) LOC;
statement.setName("statement");

statement_list = skip(skipper, *ch('\n')
                               >> ! (statement A(addChild()) % ( ch(';') | +ch('\n')))
                               >> *ch('\n'));

entryPoint = statement_list >> (eos | error("unexpected character"));
#undef LOC
#undef AP
#undef A
