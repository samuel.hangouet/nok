#pragma once
#include "PrimitiveBase.h"
#include <cmath>
namespace nok
{
    struct Int : public PrimitiveBase
    {
        Int(int n)
          : _value(n)
        { }

        Int * toInt(const NodeBase *) override { return this; }

        Double * toDouble(const NodeBase *) override;

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << _value; 
        }

        Types type() const override { return Types::Int; }

        int compare(const NodeBase * origin, const PrimitiveBase * other) const override;

        int compareSameType(const PrimitiveBase * o) const override
        {
            auto other = static_cast<const Int*>(o);
            return (other->_value < _value) - (_value < other->_value);
        }

        PrimitiveBase * neg(const NodeBase * origin) const override
        {
            return new Int(- _value);
        }

        PrimitiveBase * mul(const NodeBase * origin, const PrimitiveBase * other) const override;
        PrimitiveBase * mod(const NodeBase * origin, const PrimitiveBase * other) const override;
        PrimitiveBase * div(const NodeBase * origin, const PrimitiveBase * other) const override;
        PrimitiveBase * add(const NodeBase * origin, const PrimitiveBase * other) const override;
        PrimitiveBase * sub(const NodeBase * origin, const PrimitiveBase * other) const override;

        PrimitiveBase * shiftRight(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Int(_value >> other->toInt(origin)->_value);
        }

        PrimitiveBase * shiftLeft(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Int(_value << other->toInt(origin)->_value);
        }

        PrimitiveBase * bitAnd(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Int(_value & other->toInt(origin)->_value);
        }

        PrimitiveBase * bitOr(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Int(_value | other->toInt(origin)->_value);
        }

        PrimitiveBase * bitXor(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Int(_value ^ other->toInt(origin)->_value);
        }

        PrimitiveBase * bitInv(const NodeBase * origin) const override
        {
            return new Int(~_value);
        }

        const int _value;
    };

    struct Double : public PrimitiveBase
    {
        Double(double n)
          : _value(n)
        { }

        Double * toDouble(const NodeBase *) override { return this; }
        Int * toInt(const NodeBase *) override { return new Int(_value); }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << _value; 
        }

        Types type() const override { return Types::Double; }

        int compare(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            if (other->type() == Types::Int)
            {
                double ov = static_cast<const Int*>(other)->_value;
                return (ov < _value) - (_value < ov);
            }

            return PrimitiveBase::compare(origin, other);
        }

        int compareSameType(const PrimitiveBase * o) const override
        {
            auto other = static_cast<const Double*>(o);
            return (other->_value < _value) - (_value < other->_value);
        }

        PrimitiveBase * neg(const NodeBase * origin) const override
        {
            return new Double(- _value);
        }

        PrimitiveBase * mul(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Double(_value * other->toDouble(origin)->_value);
        }

        PrimitiveBase * mod(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Double(std::fmod(_value, other->toDouble(origin)->_value));
        }

        PrimitiveBase * div(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Double(_value / other->toDouble(origin)->_value);
        }

        PrimitiveBase * add(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Double(_value + other->toDouble(origin)->_value);
        }

        PrimitiveBase * sub(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new Double(_value - other->toDouble(origin)->_value);
        }

        const double _value;
    };

    inline Double * Int::toDouble(const NodeBase *) { return new Double(_value); }

    inline int Int::compare(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::Double)
        {
            double ov = static_cast<const Double*>(other)->_value;
            return (ov < _value) - (_value < ov);
        }
        return PrimitiveBase::compare(origin, other);
    }

    inline PrimitiveBase * Int::mul(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::Double)
            return new Double(_value * static_cast<const Double*>(other)->_value);
        return new Int(_value * other->toInt(origin)->_value);
    }

    inline PrimitiveBase * Int::mod(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::Double)
            return new Double(std::fmod(_value, static_cast<const Double*>(other)->_value));
        return new Int(_value % other->toInt(origin)->_value);
    }

    inline PrimitiveBase * Int::div(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::Double)
            return new Double(_value / static_cast<const Double*>(other)->_value);
        return new Int(_value / other->toInt(origin)->_value);
    }

    inline PrimitiveBase * Int::add(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::Double)
            return new Double(_value + static_cast<const Double*>(other)->_value);
        return new Int(_value + other->toInt(origin)->_value);
    }

    inline PrimitiveBase * Int::sub(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::Double)
            return new Double(_value - static_cast<const Double*>(other)->_value);
        return new Int(_value - other->toInt(origin)->_value);
    }
}
