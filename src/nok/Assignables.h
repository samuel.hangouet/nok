#pragma once
#include "NodeBase.h"
#include "String.h"
#include "Object.h"
namespace nok
{
    struct AssignableBase : public NodeBase
    {
        void checkWritable() const override { }
    };

    struct Variable : public AssignableBase
    {
        Variable(const std::string & name)
          : _name(name)
        { }

        PrimitiveBase * create(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return scope->create(this, & _name, value);
        }

        PrimitiveBase * change(Object * scope, const NodeBase * origin, PrimitiveBase * value, SetterCallback setter) const override
        {
            auto p = scope->lookup(& _name);
            if (p.first == nullptr)
                throw error() << "Undefined variable `" << *this << "`. Do you mean `:` instead?";
            return setter(*p.first, value);
        }

        PrimitiveBase * setOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return scope->setOrCreate(& _name, value);
        }

        PrimitiveBase * getOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return scope->getOrCreate(& _name, value);
        }

        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto p = scope->lookup(& _name);
            if (p.first == nullptr)
                throw error() << "`" << *this << "` is not defined";
            return {*p.first, p.second};
        }

        void dump(std::ostream & os) const override
        {
            os << _name.value();
        }

        Variable * asVariable() override { return this; }

        const String _name;
    };

    struct Constant : public AssignableBase
    {
        Constant(PrimitiveBase * p)
          : _primitive(p)
        { }

        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            return {_primitive, nullptr};
        }

        PrimitiveBase * create(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return scope->create(this, _primitive, value);
        }

        PrimitiveBase * change(Object * scope, const NodeBase * origin, PrimitiveBase * value, SetterCallback setter) const override
        {
            auto p = scope->lookup(_primitive);
            if (p.first == nullptr)
                throw error() << "Undefined key `" << *this << "`. Do you mean `:` instead?";
            return setter(*p.first, value);
        }

        PrimitiveBase * setOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return scope->setOrCreate(_primitive, value);
        }

        PrimitiveBase * getOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return scope->getOrCreate(_primitive, value);
        }

        void dump(std::ostream & os) const override
        {
            os << *_primitive;
        }

        Constant * asConstant() override { return this; }

        PrimitiveBase * _primitive;
    };

    struct Access : public AssignableBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto aggregate = _children[0]->eval(scope).first;
            auto value = aggregate->at(this, key(scope));
            if (aggregate->type() == Types::Object)
                return {value, static_cast<Object *>(aggregate)};
            return {value, nullptr};
        }

        Object * assignable(Object * scope) const
        {
            auto l = _children[0]->eval(scope).first;
            if (l->type() != Types::Object)
                throw _children[0]->error() << l->type() << " is not mutable";
            return static_cast<Object*>(l);
        }

        PrimitiveBase * key(Object * scope) const
        {
            return _children[1]->eval(scope).first;
        }

        PrimitiveBase * create(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return assignable(scope)->create(_children[1], key(scope), value);
        }

        PrimitiveBase * change(Object * scope, const NodeBase * keyOrigin, PrimitiveBase * value, SetterCallback setter) const override
        {
            auto k = key(scope);
            auto ptr = assignable(scope)->get(k);
            if (ptr == nullptr)
                throw keyOrigin->error() << "No key `" << *k << '`';
            return setter(*ptr, value);
        }

        PrimitiveBase * setOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return assignable(scope)->setOrCreate(key(scope), value);
        }

        PrimitiveBase * getOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return assignable(scope)->getOrCreate(key(scope), value);
        }

        void dump(std::ostream & os) const override
        {
            os << *_children[0] << "[" << *_children[1] << "]";
        }
    };

    struct Member : public AssignableBase
    {
        Object * object(Object * scope) const
        {
            auto left = _children[0]->eval(scope).first;
            return left->toObject(_children[0]);
        }

        const Variable * var() const { return static_cast<const Variable*>(_children[1]); }

        const String * key() const { return & var()->_name; }

        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto r = object(scope)->lookupMember(key());
            if (r.first == nullptr)
                throw var()->error() << "Undefined member `" << *var() << "`";
            return {*r.first, r.second};
        }

        PrimitiveBase * create(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return object(scope)->create(var(), key(), value);
        }

        PrimitiveBase * change(Object * scope, const NodeBase * origin, PrimitiveBase * value, SetterCallback setter) const override
        {
            auto p = object(scope)->lookupMember(key());
            if (p.first == nullptr)
                throw var()->error() << "Undefined member `" << *var() << '`';
            return setter(*p.first, value);
        }

        PrimitiveBase * setOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return object(scope)->setOrCreate(key(), value);
        }

        PrimitiveBase * getOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            return object(scope)->getOrCreate(key(), value);
        }

        void dump(std::ostream & os) const override
        {
            os << *_children[0] << "." << *_children[1];
        }
    };

    struct ListNode : public AssignableBase
    {
        ListNode * asListNode() override { return this; }

        void dump(std::ostream & os) const override
        {
            os << '[';
            for(int i = 0; i < (int)_children.size(); ++i)
            {
                if (i != 0)
                    os << ", ";
                _children[i]->dump(os);
            }
            os << ']';
        }

        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            std::vector<PrimitiveBase*> v;
            for (auto n: _children)
            {
                v.push_back(n->eval(scope).first);
            }
            return {new List(std::move(v)), nullptr};
        }

        std::pair<const ListNode*,const List*> assigned(const NodeBase * valueOrigin, PrimitiveBase * value) const
        {
            if (value->type() != Types::List)
                throw valueOrigin->error() << "Trying to assign " << value->type() << " to a list";
            auto valueList = static_cast<List*>(value);
            if ((int)_children.size() != valueList->size())
                throw valueOrigin->error() << "Trying to assign lists with different sizes" ;
            const ListNode * lo = const_cast<NodeBase*>(valueOrigin)->asListNode();
            if (lo == nullptr)
                lo = this;
            return {lo, valueList};
        }

        PrimitiveBase * create(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            auto [lo,l] = assigned(valueOrigin, value);
            for (int i = 0; i < (int)_children.size(); ++i)
                _children[i]->create(scope, lo->_children[i], l->_content[i]);
            return value;
        }

        PrimitiveBase * change(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value, SetterCallback setter) const override
        {
            auto [lo,l] = assigned(valueOrigin, value);
            for (int i = 0; i < (int)_children.size(); ++i)
                _children[i]->change(scope, lo->_children[i], l->_content[i], setter);
            return value;
        }

        PrimitiveBase * setOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            auto [lo,l] = assigned(valueOrigin, value);
            for (int i = 0; i < (int)_children.size(); ++i)
                _children[i]->setOrCreate(scope, lo->_children[i], l->_content[i]);
            return value;
        }

        PrimitiveBase * getOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const override
        {
            auto [lo,l] = assigned(valueOrigin, value);
            for (int i = 0; i < (int)_children.size(); ++i)
                _children[i]->getOrCreate(scope, lo->_children[i], l->_content[i]);
            return value;
        }

        void checkWritable() const override
        {
            if (_children.empty())
                throw error() << "Cannot assign empty list";
            for (auto c: _children)
                c->checkWritable();
        }
    };
}
