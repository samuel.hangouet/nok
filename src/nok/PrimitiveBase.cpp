#include "PrimitiveBase.h"
#include "String.h"
#include "List.h"
namespace nok
{
    PrimitiveBase * PrimitiveBase::concat(const NodeBase * origin, const PrimitiveBase * other) const
    {
        if (other->type() == Types::String)
            return other->toString()->concat(origin, other);

        if (other->type() != Types::List)
            throw origin->error() << "Cannot concatenate " << type() << " with " << other->type();

        auto l = new List{std::vector{const_cast<PrimitiveBase*>(this)}};
        return l->concat(origin, other);
    }

    const String * PrimitiveBase::toString() const
    {
        std::ostringstream os;
        os << *this;
        return new String(os.str());
    }
}
