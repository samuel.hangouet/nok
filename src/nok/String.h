#pragma once
#include "PrimitiveBase.h"
#include "Numbers.h"
#include "Null.h"
#include <ell/ell.h>
namespace nok
{
    struct String : public PrimitiveBase
    {
        String(const std::string & s) : _value(s) { }
        String(std::string && s) : _value(s) { }

        const String * toString() const override { return this; }
        String * toString(const NodeBase *) override { return this; }

        const std::string & value() const { return _value; }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << '"';
            ell::dump_string(os, _value);
            os << '"';
        }

        PrimitiveBase * at(const NodeBase * origin, const PrimitiveBase * index) const override
        {
            auto i = index->toInt(origin)->_value;
            if (i < 0 || i >= static_cast<int>(_value.size()))
                throw origin->error() << "Out of bounds";
            return new String(_value.substr(i, 1));
        }

        void foreach(const NodeBase * origin, std::function<bool(PrimitiveBase*)> callback) const override
        {
            for(char n: _value)
            {
                if (! callback(new String(std::string(1, n))))
                    break;
            }
        }

        PrimitiveBase * copy(const NodeBase * origin, int first, int last) const override
        {
            if (first < (int)_value.size() && last >= first)
                return new String(_value.substr(first, last - first + 1));
            return new String("");
        }

        PrimitiveBase * find(const NodeBase * origin, const PrimitiveBase * needle) const override
        {
            if (needle->type() != Types::String)
                origin->error() << "Cannot search " << needle->type() << " into a string";
            auto n = _value.find(static_cast<const String*>(needle)->_value);
            if (n == _value.npos)
                return new Null;
            return new Int(n);
        }

        PrimitiveBase * concat(const NodeBase * origin, const PrimitiveBase * other) const override
        {
            return new String(_value + other->toString()->_value);
        }

        int size(const NodeBase * origin) const override { return _value.size(); }

        Types type() const override { return Types::String; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            auto other = static_cast<const String *>(o);
            return _value.compare(other->_value);
        }

        bool validIdentifier() const
        {
            if (_value.empty())
                return false;
            char first = _value[0];
            if ((first >= '0') & (first <= '9'))
                return false;
            for (char c: _value)
            {
                if ((c > 0)
                   &((c < 'a') | (c > 'z'))
                   &((c < 'A') | (c > 'Z'))
                   &((c < '0') | (c > '9')))
                    return false;
            }
            return true;
        }

        const std::string _value;
    };
}
