#pragma once
#include <vector>
#include <unordered_set>
#include <ell/ell.h>
#include "NodeBase.h"
#include "Object.h"
#include "ObjectNode.h"
#include "Assignables.h"
#include "Operators.h"
#include "Statements.h"
#include "Call.h"
namespace std
{
    template <>
    struct hash<ell::Location>
    {
        size_t operator()(const ell::Location & l) const
        {
            size_t s0 = reinterpret_cast<size_t>(l.parent);
            size_t s1 = reinterpret_cast<size_t>(l.filename);
            size_t s2 = reinterpret_cast<size_t>(l.position);
            return (s0 * 31 + s1) * 17 + s2;
        }
    };
}
namespace nok
{
    struct Parser : public ell::Parser, public ell::Grammar
    {
        ell::Rule FIRST_CHAR, NEXT_CHAR, IDENTIFIER, INTEGRAL, STRING, STRING_CHAR,
            NUMBER, COLON, PLUS, MINUS, DOT, NOT, GT, LE, LT, AMP, BWXOR, BWOR, EQUAL,
            skipper, nl_skipper,
            listOrRange, object, atome, arg_list, access_or_copy, assignable,
            prefixed, product, sum, shift, concat, search, threeWay,
            comparison, bwand, bwxor, bwor, land, lxor, lor, base, expression,
            assignation, assign_list, statement,
            statement_list, entryPoint;

        Parser()
        {
#include "Grammar.h"
        }

        PrimitiveBase * parseFile(const std::string & path, Object * scope)
        {
            std::ifstream f(path);
            if (!f.good())
                throw Error() << "Cannot read file \"" << path << "\"";
            std::string content;
            content.assign(std::istreambuf_iterator<char>(f), std::istreambuf_iterator<char>());
            return parseRaw(content, path, scope);
        }

        PrimitiveBase * parseRaw(const std::string & content, const std::string & path, Object * scope)
        {
            auto root = parse(content, path);
            root->check();
            return root->eval(scope).first;
        }

        PrimitiveBase * parseInteractive(const std::string & content, Object * scope)
        {
            auto root = parse(content, "<stdin>");
            std::vector<PrimitiveBase*> values;
            // Separated check to allow use of interpreter like a calculator (root nodes without any side-effects)
            for (auto & node: root->_children)
            {
                node->check();
                values.push_back(node->eval(scope).first);
            }
            if (values.size() == 1)
                return values[0];
            return new List(std::move(values));
        }

        ObjectNode * parse(const std::string & content, const std::string & path)
        {
            auto target = new ObjectNode;
            push(target);
            match(entryPoint, path.c_str(), content.c_str(), content.size());
            return target;
        }

        void pushFor()
        {
            auto body = pop();
            auto iterable = pop();
            auto f = new For;
            f->addChild(pop());
            f->addChild(iterable);
            f->addChild(body);
            push(f);
        }

        void pushBinary(NodeBase * node)
        {
            auto right = pop();
            node->addChild(pop());
            node->addChild(right);
            push(node);
        }

        void pushUnary(NodeBase * node)
        {
            auto child = pop();
            node->addChild(child);
            push(node);
        }

        NodeBase * pop()
        {
            auto n = back();
            std::ostringstream oss;
            oss << "Pop " << *n;
            trace(nullptr, "|", oss.str().c_str());
            _stack.pop_back();
            return n;
        }

        template <typename Primitive, typename ...Args>
        void pushConstant(Args... args)
        {
            push(new Constant(new Primitive(args...)));
        }

        void pushAssign(AssignBase * op)
        {
            auto value = pop();
            auto assignable = pop();

            // Transform a()=>b():c into a():c=>b()
            auto base = assignable->asSetBaseOperator();
            if (base != nullptr)
            {
                assignable = base->_children[0];
                base->_children[0] = value;
                value = base;
            }

            // Transform a():b into a:[]->b
            auto call = assignable->asCall();
            if (call != nullptr)
            {
                assignable = call->_children[0];
                push(call->_children[1]);
                delete call;
                push(value);
                pushFunction();
                value = pop();
            }

            op->addChild(assignable);
            op->addChild(value);
            push(op);
        }

        void pushFunction()
        {
            auto body = pop();
            auto args = pop();

            // Check and normalize arguments list
            auto list = args->asListNode();
            if (list == nullptr)
            {
                list = new ListNode;
                if (args->asVariable() != nullptr)
                {
                    list->addChild(args);
                }
                else
                {
                    auto lit = args->asConstant();
                    if (lit == nullptr || lit->_primitive->type() != Types::Null)
                        throw args->error() << "Invalid argument list";
                }
            }
            else for (auto n: list->_children)
            {
                if (n->asVariable() == nullptr)
                    throw n->error() << "Invalid argument";
            }

            auto func = new FuncOperator;
            func->addChild(list);
            func->addChild(body);
            push(func);
        }

        // Note: the given node must already have its mandatory children
        void push(NodeBase * n)
        {
            std::ostringstream oss;
            oss << "Push " << *n;
            trace(nullptr, "|", oss.str().c_str());
            _stack.push_back(n);
        }

        void addChild()
        {
            auto n = pop();
            std::ostringstream oss;
            oss << "Add child " << *n << " to " << *back();
            trace(nullptr, "|", oss.str().c_str());
            back()->addChild(n);
        }

        NodeBase * back() { return _stack.back(); }

        // Avoids too much allocations of location strings to annotate AST nodes
        const ell::Location * pinLocation(const ell::Location & l)
        {
            auto r = _locations.insert(l);
            return &*r.first;
        }

        std::unordered_set<ell::Location> _locations;
        std::vector<NodeBase*> _stack;
        std::string _string;
        int _int;
    };
}

