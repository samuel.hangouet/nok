#pragma once
#include "Function.h"
#include <functional>
namespace nok
{
    struct Data : public PrimitiveBase
    {
        Data(void * data)
          : _data(data)
        { }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << "data";
        }

        Types type() const override { return Types::Data; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            return (o < this) - (this < o);
        }

        void * const _data;
    };

    struct NativeFunction : public PrimitiveBase
    {
        NativeFunction(PrimitiveBase * func(Object *, const NodeBase *, List *))
          : _func(func)
        { }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << "native function";
        }

        std::pair<PrimitiveBase*,Object*> call(Object * owner, const NodeBase * paramOrigin, List * param) const override
        {
            return {(_func)(owner, paramOrigin, param),nullptr};
        }

        Types type() const override { return Types::Function; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            return (o < this) - (this < o);
        }

    private:
        std::function<PrimitiveBase *(Object *, const NodeBase *, List *)> _func;
    };

    template <typename Param>
    Param get(const NodeBase * origin, PrimitiveBase * v);

    template <>
    Function* get<Function*>(const NodeBase * origin, PrimitiveBase * v) { return v->toFunction(origin); }
    template <>
    PrimitiveBase* get<PrimitiveBase*>(const NodeBase * origin, PrimitiveBase * v) { return v; }
    template <>
    Object* get<Object*>(const NodeBase * origin, PrimitiveBase * v) { return v->toObject(origin); }
    template <>
    bool get<bool>(const NodeBase * origin, PrimitiveBase * v) { return v->toBool(origin)->_value; }
    template <>
    int get<int>(const NodeBase * origin, PrimitiveBase * v) { return v->toInt(origin)->_value; }
    template <>
    const char * get<const char *>(const NodeBase * origin, PrimitiveBase * v) { return v->toString()->_value.c_str(); }
    template <>
    const std::string & get<const std::string &>(const NodeBase * origin, PrimitiveBase * v) { return v->toString()->_value; }

    template <typename R, typename... Args>
    struct BindedFunction : public PrimitiveBase
    {
        BindedFunction(R func(Args...))
          : _func(func)
        { }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << "native function";
        }

        std::pair<PrimitiveBase*,Object*> call(Object * owner, const NodeBase * paramOrigin, List * parameter) const override
        {
            if constexpr (ParametersCount == 0)
            {
                if (parameter->size() != 0)
                    throw paramOrigin->error() << "Unexpected parameter";
            }

            if constexpr (HasReturnValue)
            {
                decltype(auto) r = std::apply(_func, build<0>(paramOrigin, parameter));
                return {put(r),nullptr};
            }
            else
            {
                std::apply(_func, build<0>(paramOrigin, parameter));
                return {new Null,nullptr};
            }
        }

        Types type() const override { return Types::Function; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            return (o < this) - (this < o);
        }

    private:
        static constexpr int ParametersCount = sizeof...(Args);

        static constexpr bool HasReturnValue = !(std::is_same<R,void>::value);

        template <const int N>
        static auto build(const NodeBase * origin, List * parameter)
        {
            if constexpr (N < sizeof...(Args))
            {
                auto nextParams = build<N+1>(origin, parameter);
                PrimitiveBase * value = parameter->at(origin, N);
                return std::tuple_cat(std::make_tuple(get<typename std::tuple_element<N,std::tuple<Args...>>::type>(origin, value)), nextParams);
            }
            else
                return std::tuple<>();
        }

        static PrimitiveBase * put(bool v) { if (v) return new Bool(true); return new Bool(false); }
        static PrimitiveBase * put(int v) { return new Int(v); }
        static PrimitiveBase * put(float v) { return new Double(v); }
        static PrimitiveBase * put(double v) { return new Double(v); }
        static PrimitiveBase * put(const std::string & s) { return new String(s); }
        static PrimitiveBase * put(const char * & s) { return new String(s); }
        static PrimitiveBase * put(PrimitiveBase * p) { return p; }

        std::function<R(Args...)> _func;
    };

    template <typename R, typename... Args>
    BindedFunction(R func(Args...)) -> BindedFunction<R, Args...>;
}
