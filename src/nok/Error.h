#pragma once
#include <ell/ell.h>
namespace nok
{
    struct PrimitiveBase;

    struct Error : public ell::Error
    {
        Error(PrimitiveBase * context = nullptr)
          : ell::Error(""),
            _context(context)
        { }

        template <typename T>
        Error & operator << (const T & t)
        {
            std::ostringstream os;
            os << t;
            _msg += os.str();
            return * this;
        }

        PrimitiveBase * _context;
    };
}
