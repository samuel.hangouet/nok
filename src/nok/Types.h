#include <iostream>
#pragma once
#define NOK_TYPES T(Null) T(String) T(Bool) T(Int) T(Double) T(Object) T(List) T(Function) T(Range) T(Type) T(Data)
namespace nok
{
    enum class Types
    {
#define T(Type) Type,
        NOK_TYPES
#undef T
    };

#define T(Type) struct Type;
        NOK_TYPES
#undef T

    inline std::ostream & operator << (std::ostream & os, Types t)
    {
        switch(t)
        {
#define T(Type) case Types::Type: os << std::tolower(#Type[0]) << (#Type+1); break;
        NOK_TYPES
#undef T
        }
        return os;
    }

}
