#pragma once
#include "PrimitiveBase.h"
#include "Assignables.h"
namespace nok
{
    struct Function : public PrimitiveBase
    {
        Function(Object * scope, const ListNode * parameters, NodeBase * body)
          : _scope(scope),
            _parameters(parameters),
            _body(body)
        { }

        Function * toFunction(const NodeBase * origin) override { return this; }

        Types type() const override { return Types::Function; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            return (o < this) - (this < o);
        }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parent) const override
        {
            os << * _parameters << "->" << * _body;
        }

        std::pair<PrimitiveBase*,Object*> call(Object * owner, const NodeBase * paramOrigin, List * parameters) const override
        {
            TRACE("Call " << *this << " with " << *parameter);

            auto newScope = new InvisibleObject(_scope);
            _parameters->create(newScope, paramOrigin, parameters);
            if (owner != nullptr)
            {
                newScope->setOrCreate(new String("this"), owner);
                newScope->_parent = owner;
            }
            newScope->_locked = true;

            TRACE("Created parameters in scope " << *newScope);

            auto v = _body->eval(newScope);
            if (newScope->_interrupt.kind == Interrupt::Return)
                v = {newScope->_interrupt.returnValue, newScope->_interrupt.owner};
            else if (v.first == nullptr)
                v = {new Null, nullptr};
            TRACE("=> Result is " << *v.first);
            return v;
        }

    private:
        Object * _scope;
        const ListNode * _parameters;
        const NodeBase * _body;
    };
}
