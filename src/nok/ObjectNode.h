#pragma once
#include "NodeBase.h"
#include "List.h"
#include "Null.h"
#include "Operators.h"
namespace nok
{
    struct ObjectNode : public NodeBase
    {
        void dump(std::ostream & os) const override
        {
            os << "{ ";
            for(int i = 0; i < (int)_children.size(); ++i)
            {
                if (i != 0)
                    os << "; ";
                _children[i]->dump(os);
            }
            os << " }";
        }

        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto newObject = new Object(scope);
            for (auto n: _children)
            {
                n->eval(newObject);

                if (newObject->_interrupt.kind != Interrupt::None)
                {
                    scope->_interrupt = newObject->_interrupt;
                    return {nullptr,nullptr};
                }             
            }
            return {newObject,nullptr};
        }

        bool isStatement(NodeBase * n) const
        {
            if (n->asStatementBase() != nullptr ||
                n->asCall() != nullptr ||
                n->asAssignBase() != nullptr ||
                n->asSetBaseOperator() != nullptr)
                return true;

            if (n->asListNode() != nullptr)
            {
                for(auto c: n->_children)
                {
                    if (! c->asAssignBase())
                        return false;
                }
                return true;
            }

            return false;
        }

        void check() const override
        {
            for (auto c: _children)
            {
                if (! isStatement(c))
                    throw c->error() << "Unused expression: " << *c;
                c->check();
            }
        }
    };
}
