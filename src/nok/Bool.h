#pragma once
#include "PrimitiveBase.h"
namespace nok
{
    struct Bool : public PrimitiveBase
    {
        Bool(bool value)
          : _value(value)
        { }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << (_value ? "true" : "false");
        }

        Types type() const override { return Types::Bool; }

        int compareSameType(const PrimitiveBase * o) const override
        {
            auto other = static_cast<const Bool *>(o);

            return (other->_value < _value) - (_value < other->_value);
        }

        Bool * toBool(const NodeBase *) override { return this; }

        const bool _value;
    };
}
