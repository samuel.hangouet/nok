#pragma once
#include "PrimitiveBase.h"
namespace nok
{
    struct Null : public PrimitiveBase
    {
        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << "()";
        }

        Null * toNull(const NodeBase *) override { return this; }

        Types type() const override { return Types::Null; }

        int compareSameType(const PrimitiveBase * other) const override { return 0; }
    };
}
