#pragma once
#include <unordered_set>
#include <functional>
#include "Types.h"
#include "Error.h"
#include "NodeBase.h"
namespace nok
{
    struct PrimitiveBase
    {
        virtual std::pair<PrimitiveBase*,Object*> call(Object * owner, const NodeBase * paramOrigin, List * parameter) const
        {
            throw paramOrigin->error() << "Not callable";
        }

        virtual void foreach(const NodeBase * origin, std::function<bool(PrimitiveBase*)> callback) const
        {
            throw origin->error() << "Not iterable";
        }

        virtual PrimitiveBase * copy(const NodeBase * origin, int first, int last) const
        {
            throw origin->error() << "Cannot take range from " << type();
        }

        virtual PrimitiveBase * concat(const NodeBase * origin, const PrimitiveBase * other) const;

        virtual PrimitiveBase * neg(const NodeBase * origin) const
        {
            throw origin->error() << "Unary `-` not defined for " << type();
        }

        virtual PrimitiveBase * bitInv(const NodeBase * origin) const
        {
            throw origin->error() << "`~` not defined for " << type();
        }

        virtual PrimitiveBase * mul(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`*` not defined for " << type();
        }

        virtual PrimitiveBase * mod(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`%` not defined for " << type();
        }

        virtual PrimitiveBase * div(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`/` not defined for " << type();
        }

        virtual PrimitiveBase * add(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`+` not defined for " << type();
        }

        virtual PrimitiveBase * sub(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`-` not defined for " << type();
        }

        virtual PrimitiveBase * shiftRight(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`>>` not defined for " << type();
        }

        virtual PrimitiveBase * shiftLeft(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`<<` not defined for " << type();
        }

        virtual PrimitiveBase * bitAnd(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`&` not defined for " << type();
        }

        virtual PrimitiveBase * bitOr(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`|` not defined for " << type();
        }

        virtual PrimitiveBase * bitXor(const NodeBase * origin, const PrimitiveBase * other) const
        {
            throw origin->error() << "`^` not defined for " << type();
        }

        virtual PrimitiveBase * find(const NodeBase * origin, const PrimitiveBase * needle) const
        {
            throw origin->error() << "`@` not defined for " << type();
        }

        virtual Types type() const = 0;

        virtual int size(const NodeBase * origin) const
        {
            throw origin->error() << "Not an aggregate";
        }

        virtual PrimitiveBase * at(const NodeBase * origin, const PrimitiveBase * keyOrIndex) const
        {
            throw origin->error() << "Not an aggregate";
        }

        virtual void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const = 0;

        friend std::ostream& operator << (std::ostream & os, const PrimitiveBase & p)
        {
            std::unordered_set<const PrimitiveBase*> parents;
            p.dump(os, parents);
            return os;
        }

        virtual int compare(const NodeBase * origin, const PrimitiveBase * other) const
        {
            auto t = type();
            auto ot = other->type();
            if (t != ot)
            {
                if (t == Types::Null)
                    return -1;
                if (ot == Types::Null)
                    return 1;
                throw origin->error() << "Cannot compare " << type() << " with " << other->type();
            }
            return compareSameType(other);
        }

        virtual int compareSameType(const PrimitiveBase * o) const = 0;

        virtual const String * toString() const;

#define T(Class)                                                         \
        virtual Class * to##Class(const NodeBase * origin)               \
        {                                                                \
            throw origin->error() << "Expecting " << Types::Class        \
                                  << " got " << type();                  \
        }                                                                \
        const Class * to##Class(const NodeBase * origin) const           \
        {                                                                \
            return const_cast<PrimitiveBase*>(this)->to##Class(origin);  \
        }
        NOK_TYPES
#undef T
    };
}
