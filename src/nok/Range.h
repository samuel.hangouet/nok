#pragma once
#include "PrimitiveBase.h"
namespace nok
{
    struct Range : public PrimitiveBase
    {
        Range(int from, int to, int step)
          : _from(from), _to(to), _step(step)
        { }

        void foreach(const NodeBase * origin, std::function<bool(PrimitiveBase*)> callback) const override
        {
            if (_step > 0)
            {
                for (int i = _from; i <= _to; ++i)
                {
                    if (! callback(new Int(i)))
                        break;
                }
                return;
            }

            for (int i = _from; i >= _to; --i)
            {
                if (!  callback(new Int(i)))
                    break;
            }
        }

        Types type() const override { return Types::Range; }

        void dump(std::ostream & os, std::unordered_set<const PrimitiveBase *> & parents) const override
        {
            os << '[' << _from << ':' << _to;
            if (_step != 1)
                os << ':' << _step;
            os << ']';
        }

        int compareSameType(const PrimitiveBase * other) const override
        {
            auto o = static_cast<const Range *>(other);
            int f = (o->_from < _from) - (_from < o->_from);
            if (f != 0)
                return f;
            return (o->_to < _to) - (_to < o->_to);
        }

        const int _from;
        const int _to;
        const int _step;
    };
}
