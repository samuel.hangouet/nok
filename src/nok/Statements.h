#pragma once
#include "NodeBase.h"
#include "Null.h"
namespace nok
{
    // A statement is a node that cannot be a child of an operator,
    // and whose eval returns nullptr.
    struct StatementBase : public NodeBase
    {
        StatementBase * asStatementBase() override { return this; }
    };

    struct Return : public StatementBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            std::pair<PrimitiveBase*,Object*> p;
            if (_children.empty())
                p = {new Null,nullptr};
            else
                p = _children[0]->eval(scope);

            scope->_interrupt = { Interrupt::Return, p.first, p.second };
            return {nullptr,nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "return";
            if (! _children.empty())
                os << ' ' << *_children[0];
        }

        void check() const override
        { 
            NodeBase::check();
            for (NodeBase * parent = _parent; parent != nullptr; parent = parent->_parent)
            {
                if (parent->asFuncOperator() != nullptr)
                    return;
            }
            throw error() << "Invalid return outside of a function";
        }
    };

    struct Print : public StatementBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            if (! _children.empty())
            {
                auto v = _children[0]->eval(scope).first;
                std::cout << *v;
            }

            std::cout << '\n';
            return {nullptr,nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "print";
            if (! _children.empty())
                os << ' ' << *_children[0];
        }
    };

    struct Throw : public StatementBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            std::ostringstream os;
            PrimitiveBase * arised = nullptr;
            if (! _children.empty())
            {
                arised = _children[0]->eval(scope).first;
                os << *arised;
            }
            throw error(arised) << os.str();
        }

        void dump(std::ostream & os) const override
        {
            os << "throw";
            if (!_children.empty())
                os << ' ' << * _children[0];
        }
    };

    struct If : public StatementBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto cond = _children[0]->eval(scope).first;
            if (cond->toBool(_children[0])->_value)
                return _children[1]->eval(scope);

            if (hasFalseBlock())
                return _children[2]->eval(scope);

            return {nullptr,nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "if " << *_children[0] << " " << *_children[1];
            if (hasFalseBlock())
                os << " else " << *_children[2];
        }

        bool hasFalseBlock() const { return _children.size() == 3; }
    };


    struct LoopBase : public StatementBase
    {
        LoopBase * asLoopBase() override { return this; }
    };

    struct For : public LoopBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto iterable = _children[1]->eval(scope).first;
            auto newScope = new InvisibleObject(scope);

            iterable->foreach(_children[1], [&](PrimitiveBase * value){
                newScope->_locked = false;
                _children[0]->setOrCreate(newScope, _children[1], value);
                newScope->_locked = true;
                _children[2]->eval(newScope);
                if (newScope->_interrupt.kind != Interrupt::None)
                {
                    scope->_interrupt = newScope->_interrupt;
                    return newScope->_interrupt.kind == Interrupt::Continue;
                }
                return true;
            });
            return {nullptr,nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "for " << *_children[0] << ": " << *_children[1] << " " << *_children[2];
        }
    };

    struct While : public LoopBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            while (_children[0]->eval(scope).first->toBool(_children[0])->_value)
            {
                _children[1]->eval(scope);

                switch (scope->_interrupt.kind)
                {
                    case Interrupt::None:
                    case Interrupt::Continue:
                        continue;

                    case Interrupt::Break:
                    case Interrupt::Return:
                        return {nullptr, nullptr};
                }
            }
            return {nullptr, nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "while " << *_children[0] << " " << *_children[1];
        }
    };

    struct Break : public StatementBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            scope->_interrupt = { Interrupt::Break };

            return {nullptr,nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "break";
        }

        void check() const override
        { 
            NodeBase::check();
            for (NodeBase * parent = _parent; parent != nullptr; parent = parent->_parent)
            {
                if (parent->asLoopBase() != nullptr)
                    return;
            }
            throw error() << "Invalid break outside of loop";
        }
    };

    struct Continue : public StatementBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            scope->_interrupt = { Interrupt::Continue };

            return {nullptr,nullptr};
        }

        void dump(std::ostream & os) const override
        {
            os << "continue";
        }

        void check() const override
        { 
            NodeBase::check();
            for (NodeBase * parent = _parent; parent != nullptr; parent = parent->_parent)
            {
                if (parent->asLoopBase() != nullptr)
                    return;
            }
            throw error() << "Invalid continue outside of loop";
        }
    };
}
