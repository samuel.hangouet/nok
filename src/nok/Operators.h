#pragma once
#include "Object.h"
#include "Range.h"
#include "NodeBase.h"
#include "Bool.h"
#include "Numbers.h"
#include "Function.h"
#include "Assignables.h"
namespace nok
{
    struct OperatorBase : public NodeBase
    {
        void dump(std::ostream & os) const override
        {
            auto [prefix,separator,suffix] = markers();

            auto needParens = false;
            if (_parent != nullptr)
            {
                auto parentOp = _parent->asOperatorBase();
                if (parentOp != nullptr)
                    // TODO: check for associativity
                    needParens = parentOp->priority() > priority();
            }

            if (needParens)
                os << '(';
            os << prefix;
            for(int i = 0; i < (int)_children.size(); ++i)
            {
                if (i != 0)
                    os << separator;
                os << * _children[i];
            }
            os << suffix;
            if (needParens)
                os << ')';
        }

        virtual std::tuple<const char*,const char*, const char*> markers() const = 0;

        virtual int priority() const = 0;

        OperatorBase * asOperatorBase() override { return this; }
    };

    struct AssignBase : public OperatorBase
    {
        AssignBase * asAssignBase() override { return this; }

        void check() const override
        {
            NodeBase::check();
            _children[0]->checkWritable();
        }
    };

#define OP(BASE, PRIORITY, NAME, PFX, SEP, SFX, EVAL, ...)                    \
    struct NAME##Operator : public BASE##Base                                 \
    {                                                                         \
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override \
        { EVAL; }                                                             \
        __VA_ARGS__                                                           \
    protected:                                                                \
        int priority() const override { return PRIORITY; }                    \
        std::tuple<const char*,const char*, const char*>                      \
            markers() const override { return {PFX, SEP, SFX}; }              \
    };

#define L _children[0]->eval(scope).first
#define R _children[1]->eval(scope).first
#define RV(V) return (std::pair<PrimitiveBase*,Object*>(V,nullptr))
#define I(i) _children[i]->eval(scope).first->toInt(_children[i])->_value
OP(Operator, 18, Copy, "", "", "", 
            auto ag = L;
            int size = ag->size(_children[0]);
            int first = R->toInt(_children[1])->_value;
            int last = size - 1;
            if (_children.size() > 2)
                last = _children[2]->eval(scope).first->toInt(_children[2])->_value;
            while (first < 0) first += size;
            while (last < 0) last += size;
            RV(ag->copy(_children[0], first, last)),
        void dump(std::ostream & os) const override
        {
            os << *_children[0] << "[" << *_children[1] << ":";
            if (_children.size() == 3)
                os << * _children[2];
            os << "]";
        }
)
OP(Operator, 18, Range, "[", ":", "]", RV(new Range(I(0), I(1), _children.size() == 3 ? I(2) : 1)))
OP(Operator, 17, GetBase, "", "", ".>", RV(L->toObject(_children[0])->getBase(this)))
OP(Operator, 16, Lnot, "!", "", "", RV(new Bool(! L->toBool(_children[0])->_value)))
OP(Operator, 16, Minus, "-", "", "", RV(L->neg(this)))
OP(Operator, 16, Size, "#", "", "", RV(new Int(L->size(this))))
OP(Operator, 16, BitInv, "~", "", "", RV(L->bitInv(this)))
OP(Operator, 15, Mul, "", "*", "", RV(L->mul(this, R)))
OP(Operator, 15, Modulo, "", "%", "", RV(L->mod(this, R)))
OP(Operator, 15, Div, "", "/", "", RV(L->div(this, R)))
OP(Operator, 14, Add, "", "+", "", RV(L->add(this, R)))
OP(Operator, 14, Sub, "", "-", "", RV(L->sub(this, R)))
OP(Operator, 13, ShiftLeft, "", "<<", "", RV(L->shiftLeft(this, R)))
OP(Operator, 13, ShiftRight, "", ">>", "", RV(L->shiftRight(this, R)))
OP(Operator, 12, Concat, "", "..", "", RV(L->concat(this, R)))
OP(Operator, 11, Search, "", "@", "", RV(R->find(this, L)))
OP(Operator, 10, Compare, "", "<=>", "", RV(new Int(L->compare(this, R))))
OP(Operator, 9, Eq, "", "==", "", RV(new Bool(L->compare(this, R) == 0)))
OP(Operator, 9, Neq, "", "!=", "", RV(new Bool(L->compare(this, R) != 0)))
OP(Operator, 9, Ge, "", ">=", "", RV(new Bool(L->compare(this, R) >= 0)))
OP(Operator, 9, Gt, "", ">", "", RV(new Bool(L->compare(this, R) > 0)))
OP(Operator, 9, Le, "", "<=", "", RV(new Bool(L->compare(this, R) <= 0)))
OP(Operator, 9, Lt, "", "<", "", RV(new Bool(L->compare(this, R) < 0)))
OP(Operator, 8, BitAnd, "", "&", "", RV(L->bitAnd(this, R)))
OP(Operator, 7, BitXor, "", "^", "", RV(L->bitXor(this, R)))
OP(Operator, 6, BitOr, "", "|", "", RV(L->bitOr(this, R)))
OP(Operator, 5, LogicalAnd, "", "&&", "", RV(new Bool(L->toBool(_children[0])->_value && R->toBool(_children[1])->_value)))
OP(Operator, 4, LogicalXor, "", "^^", "", RV(new Bool(L->toBool(_children[0])->_value ^ R->toBool(_children[1])->_value)))
OP(Operator, 3, LogicalOr, "", "||", "", RV(new Bool(L->toBool(_children[0])->_value || R->toBool(_children[1])->_value)))
OP(Operator, 2, SetBase, "", "=>", "", auto l = L; l->toObject(_children[0])->setBase(this, R->toObject(_children[1])); RV(l),
    SetBaseOperator * asSetBaseOperator() override { return this; })
OP(Operator, 1, Func, "", "->", "", RV(new Function(scope, static_cast<const ListNode*>(_children[0]), _children[1])),
    FuncOperator * asFuncOperator() override { return this; })
OP(Assign, 0, Create, "", ":", "", RV(_children[0]->create(scope, _children[1], R)))
OP(Assign, 0, Set, "", "=", "", RV(_children[0]->change(scope, _children[1], R, [](PrimitiveBase*&ref,PrimitiveBase*v){return ref = v;})))
OP(Assign, 0, SetOrCreate, "", ":=", "", RV(_children[0]->setOrCreate(scope, _children[1], R)))
OP(Assign, 0, GetOrCreate, "", ":?", "", RV(_children[0]->getOrCreate(scope, _children[1], R)))
OP(Assign, 0, Inc, "", "", "++", RV(_children[0]->change(scope, _children[0], L, [this](PrimitiveBase*&ref,PrimitiveBase*v){ return ref = new Int(ref->toInt(_children[0])->_value + 1); })))
OP(Assign, 0, Dec, "", "", "--", RV(_children[0]->change(scope, _children[0], L, [this](PrimitiveBase*&ref,PrimitiveBase*v){ return ref = new Int(ref->toInt(_children[0])->_value - 1); })))

#undef R
#undef L
#undef OP
}
