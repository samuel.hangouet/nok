#pragma once
namespace nok
{
    struct Call : public NodeBase
    {
        std::pair<PrimitiveBase*,Object*> eval(Object * scope) const override
        {
            auto r = _children[0]->eval(scope);
            auto paramList = static_cast<List*>(_children[1]->eval(scope).first);

            return r.first->call(r.second, _children[1], paramList);
        }

        Call * asCall() override { return this; }

        void dump(std::ostream & os) const override
        {
            _children[0]->dump(os);
            auto & params = _children[1]->_children;
            os << '(';
            for(auto c: params)
            {
                if (c != *params.begin())
                    os << ", ";
                c->dump(os);
            }
            os << ')';
        }
    };
}
