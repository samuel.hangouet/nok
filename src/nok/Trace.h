#pragma once
#include <iostream>
namespace nok
{
#define NOK_JOIN2(x, y) x ## y
#define NOK_JOIN(x, y) NOK_JOIN2(x, y)
#define TRACE(...) //nok::Autoindent NOK_JOIN(nai,__COUNTER__); std::cerr << std::string(2*Autoindent::level(), ' ') << __VA_ARGS__ << '\n'

    struct Autoindent
    {
        Autoindent() { level()++; }
        ~Autoindent() { level()--; }
        static int& level()
        {
            static int level;
            return level;
        }
    };
}
