#pragma once
#include "Error.h"
#include "Trace.h"
namespace nok
{
    struct OperatorBase;
    struct FuncOperator;
    struct Call;
    struct PrimitiveBase;
    struct Object;
    struct AssignBase;
    struct ListNode;
    struct Variable;
    struct Constant;
    struct StatementBase;
    struct SetBaseOperator;
    struct LoopBase;
    struct If;

    struct NodeBase
    {
        NodeBase(NodeBase * parent = nullptr)
          : _parent(parent),
            _location(nullptr)
        { }

        virtual ~NodeBase()
        { }

        virtual std::pair<PrimitiveBase *, Object *> eval(Object * scope) const = 0;

        virtual void dump(std::ostream & os) const = 0;

#define CONV(Class) virtual Class * as##Class() { return nullptr; }
        CONV(StatementBase)
        CONV(OperatorBase)
        CONV(FuncOperator)
        CONV(AssignBase)
        CONV(Call)
        CONV(ListNode)
        CONV(Variable)
        CONV(Constant)
        CONV(SetBaseOperator)
        CONV(LoopBase)
        CONV(If)
#undef CONV

        Error error(PrimitiveBase * context = nullptr) const
        {
            Error e(context);
            auto l = location();
            if (l != nullptr)
                e << *l << ": ";
            e << "error: ";
            return e;
        }

        const ell::Location * location() const
        {
            for (auto node = this; node != nullptr; node = node->_children.empty() ? nullptr : node->_children[0])
                if (node->_location != nullptr)
                    return node->_location;
            for (auto parent = _parent; parent != nullptr; parent = parent->_parent)
                if (parent->_location != nullptr)
                    return parent->_location;
            return nullptr;
        }

        virtual void checkWritable() const { throw error() << "Not assignable"; }

        typedef std::function<PrimitiveBase*(PrimitiveBase *&, PrimitiveBase*)> SetterCallback;

        // Base methods cannot be called thanks to semantic check
        virtual PrimitiveBase * create(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const { throw; }
        virtual PrimitiveBase * change(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value, SetterCallback setter) const { throw; }
        virtual PrimitiveBase * setOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const { throw; }
        virtual PrimitiveBase * getOrCreate(Object * scope, const NodeBase * valueOrigin, PrimitiveBase * value) const { throw; }

        friend std::ostream& operator << (std::ostream & os, const NodeBase & n)
        {
            n.dump(os);
            return os;
        }

        void addChild(NodeBase * child)
        {
            child->_parent = this;
            _children.push_back(child);
        }

        // Perform semantic check
        virtual void check() const
        {
            for(auto n: _children)
                n->check();
        }

        NodeBase * _parent;
        const ell::Location * _location;
        std::vector<NodeBase*> _children;
    };
}
