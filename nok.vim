" Vim syntax file
" Language:	Nok
" Maintainer:	Samuel Hangouët <samuel.hangouet@gmail.com>

if exists("b:current_syntax")
  finish
endif

syn case match
syn match nokIdent /\h\w*/
syn keyword	nokAtomeKw  true false null this
syn keyword	nokOpKw  and xor or not
syn keyword	nokStmtKw  return throw if print for break continue while try catch else
syn region  nokComment	start="'" end="$"
syn region  nokComment	start="'''" end="'''"
syn region nokString display start=/"/ end=/"/ skip=/\\"/ contains=nokEscape
syn region nokBlock start=/{/ end=/}/ contains=ALLBUT,nokEscape
syn region nokParens start=/(/ end=/)/ contains=ALLBUT,nokEscape
syn region nokSquare start=/\[/ end=/]/ contains=ALLBUT,nokEscape
syn match nokEscape display contained "\\\([abfnrtv\\'"?]\|[0-7][0-7][0-7]\|x[0-9A-F][0-9A-F]\)"

syn match nokNumbers display transparent "\<\d\|\.\d" contains=nokDecimal,nokHexa,nokFloat,nokOctalError,nokOctal
syn match nokDecimal display contained "\d\+\>"
syn match nokHexa display contained "0x\x\+\>"
syn match nokOctal display contained "0\o\+\>"
syn match nokOctalError	display contained "0\o*[89]\d*"
syn match nokFloat display contained "\d\+\.\d*\(e[-+]\=\d\+\)\=\>"
syn match nokFloat display contained "\.\d\+\(e[-+]\=\d\+\)\=\>"
syn match nokFloat display contained "\d\+e[-+]\=\d\+\>"

hi def link nokAtomeKw special
hi def link nokOpKw operator
hi def link nokStmtKw statement
hi def link nokComment comment
hi def link nokString string
hi def link nokEscape special
hi def link nokDecimal number
hi def link nokHexa number
hi def link nokOctal number
hi def link nokOctalError error
hi def link nokFloat number

let b:current_syntax = "nok"
