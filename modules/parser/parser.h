#include <nok/Binding.h>

using namespace nok;

namespace modules::parser
{
    Object * load()
    {
        auto scope = new Object(nullptr);

        scope->create("parse", new BindedFunction<PrimitiveBase*,const std::string &,Object*>(
            [](const std::string & content, Object * scope){
                static Parser parser;
                return parser.parseInteractive(content, scope);
            }));

        return scope;
    }
}
