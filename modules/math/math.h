#include <random>
#include <nok/Binding.h>

namespace modules::math
{
    int rand_int(int from, int to)
    {
        static std::ranlux48_base _randomEngine;
        return std::uniform_int_distribution<int>(from, to)(_randomEngine);
    }

    nok::Object * load()
    {
        auto scope = new nok::Object(nullptr);

        scope->create("rand_int", new nok::BindedFunction(rand_int));

        return scope;
    }
}
