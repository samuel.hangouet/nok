#include "linenoise.h"
#include <nok/Binding.h>

using namespace nok;

namespace modules::console
{
    Function * nok_callback;

    void callback(const char * s, linenoiseCompletions * state)
    {
        auto r = nok_callback->call(nullptr, nullptr, new List(std::vector<PrimitiveBase*>{new String(s)}));

        auto list = r.first->toList(nullptr);

        for (auto n : list->_content)
        {
            linenoiseAddCompletion(state, n->toString()->_value.c_str());
        }
    }

    String * prompt(const char * p)
    {
        auto s = linenoise(p);
        auto nok_s = new String(s);
        linenoiseFree(s);
        return nok_s;
    }

    Object * load()
    {
        auto scope = new Object(nullptr);

        scope->create("prompt", new BindedFunction(prompt));
        scope->create("clear", new BindedFunction(linenoiseClearScreen));

        auto history = new Object(nullptr);
        scope->create("history", history);
        history->create("add", new BindedFunction(linenoiseHistoryAdd));
        history->create("save", new BindedFunction(linenoiseHistorySave));
        history->create("load", new BindedFunction(linenoiseHistoryLoad));

        scope->create("setCompletionCallback", new BindedFunction<void,Function*>([](Function*cb){
            nok_callback = cb;
            linenoiseSetCompletionCallback(callback);
        }));

        return scope;
    }
}
