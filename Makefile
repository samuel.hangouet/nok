CFLAGS = -Werror -std=c++20 -Imodules -Isrc -g -D_GLIBCXX_DEBUG -Wall -fno-inline -O0
LDFLAGS =
FILES = $(wildcard *.cpp src/*/* modules/*/*)
CC = g++

test: disableTee clearLogs $(sort $(wildcard tests/*)) enableTee
	@(\
        failed=`ls out/*.log 2> /dev/null|wc -l`;\
        total=`ls tests/*.nok tests/*.err|wc -l`;\
        ok=`expr $$total - $$failed`;\
        echo "PASSED: $$ok/$$total=`expr 100 \* $$ok / $$total`%")

bin/nok: $(patsubst %.cpp,bin/%.o,$(filter %.cpp,$(FILES)))
	ccache $(CC) $^ -o $@ $(LDFLAGS)

bin/%.o: %.cpp $(filter %.h,$(FILES)) $(MAKEFILE_LIST)
	@mkdir -p $(dir $@)
	ccache $(CC) -c $< -o $@ $(CFLAGS) 

%.nok: bin/nok .always
	$< $@

$(wildcard tests/*): bin/nok
	@(\
        base=$(notdir $@);\
        if [ -e out/no-tee ]; then\
            $< $@ > out/$$base.log 2>&1; echo $$? > out/$$base.status;\
        else\
            ($< $@ 2>&1; echo $$? > out/$$base.status) | tee out/$$base.log;\
        fi;\
        test $$(cat out/$$base.status) -eq 0 && (echo "\033[32mOK:    $@\033[0m" && rm out/$$base.log) || echo "\033[31mError: $@\033[0m";\
        rm out/$$base.status)

clearLogs:
	@rm -rf out/*.log

disableTee:
	@mkdir -p out
	@touch out/no-tee

enableTee:
	@rm out/no-tee

clean:
	@rm -rf `cat .gitignore`

.PHONY: test $(wildcard *.nok tests/*) clearLogs disableTee enableTee .always
.NOTPARALLEL:
