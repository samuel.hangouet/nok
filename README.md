# NOK language

Some key points:
  - concise
  - explicit memory ownership like in rust
  - simple to learn
  - easy to embed into or interface with C++ applications
  - properties to reduce common programming errors:
      - distinction between variable creation and overwriting
      - no automatic type conversion
  - utf8 variables allowed
  - built-in classes
  - performant containers operations

## Examples of anoing details which make we work on this project

 - Why relative import cannot work out of the box ?
 - Why packages and python modules names do not match ?
 - Why to add .items when iterating on a dict ?
 - Why not an efficient bulitin array type ?
 - Why the documentation is such a mess ?
 - Why for loops induction variables can still be accessed after the loop ?
 - Why we cannot detect typo in variable assignment ?

## Build on Debian/Ubuntu Linux

```
$ sudo apt install make git ccache g++ libncurses-dev
$ make
```
